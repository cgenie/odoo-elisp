(require 'ivy)
(require 'timezone)
(require 'url)
(require 'url-http)

(setq url-debug t)

(defun odoo/-http-post-charset-name (charset)
  (symbol-name charset))

;; https://stackoverflow.com/questions/1286463/how-do-i-get-the-number-of-days-in-the-month-specified-by-an-elisp-time
(defun odoo/-days-in-month-at-time (time)
  "Return number of days in month at TIME."
  (let* ((d-time (if (stringp time) (parse-time-string time) time)))
    (timezone-last-day-of-month (nth 4 d-time) (nth 5 d-time))))

(defun odoo/-alist-to-hash (al)
  (let* ((ret (make-hash-table :test 'equal))
         (-seq (seq-do (lambda (pair) (puthash (car pair) (cdr pair) ret)) al)))
    ret))

(defun odoo/-get-assoc (key al)
  (cdr (assoc key al)))

(defun odoo/-http-post-simple-internal (url data charset extra-headers)
  (let ((url-request-method        "POST")
        (url-request-data          data)
        (url-request-extra-headers extra-headers)
        (url-mime-charset-string   (odoo/-http-post-charset-name charset)))
    (let (header
          data
          status)
      (with-current-buffer
          (url-retrieve-synchronously url)
        ;; status
        (setq status url-http-response-status)
        ;; return the header and the data separately
        (goto-char (point-min))
        (if (search-forward-regexp "^$" nil t)
            (setq header (buffer-substring (point-min) (point))
                  data   (buffer-substring (1+ (point)) (point-max)))
          ;; unexpected situation, return the whole buffer
          (setq data (buffer-string))))
      (values data header status))))

;; default values
(defun odoo/default-host ()
  odoo-host)
(defun odoo/default-user ()
  odoo-user)
(defun odoo/default-password ()
  odoo-password)
(defun odoo/default-db ()
  odoo-db)

(defun odoo/jsonrpc-call (url &optional data)
  (let* ((headers '(("Accept" . "application/json") ("Content-Type" . "application/json")))
         (params (if data `((params . ,data) (method . "call")) '()))
         (json-data (json-encode (cons `(jsonrpc . "2.0") params))))
    (odoo/-http-post-simple-internal url json-data nil headers)))

(defun odoo/jsonrpc-call-with-decode (url &optional data)
  (let* ((ret (odoo/jsonrpc-call url data))
         (decoded(json-read-from-string (car ret)))
         (result (odoo/-get-assoc 'result decoded))
         (err (odoo/-get-assoc 'error decoded)))
    (if err
        (throw 'jsonrpc-error err)
        result)))

(defun odoo/databases (host)
  (let ((path "/web/database/list"))
    (odoo/jsonrpc-call-with-decode (concat host path))))

(defun odoo/login (host login password db)
  (let* ((path "/web/session/authenticate")
         (data `((login . ,login) (password . ,password) (db . ,db) (context . ,(make-hash-table :test 'equal)))))
    (odoo/jsonrpc-call-with-decode (concat host path) data)))

(defun odoo/get-session-info (host)
  (let ((path "/web/session/get_session_info"))
    (odoo/jsonrpc-call-with-decode (concat host path))))

(defun odoo/logout (host)
  (let ((path "/web/session/logout"))
    (odoo/jsonrpc-call-with-decode (concat host path))))

(defun odoo/call-kw (host model method &optional args kwargs)
  (let* ((path "/web/dataset/call_kw")
         (d-args (if args args []))
         (d-kwargs (if kwargs kwargs #s(hash-table test equal)))
         (data `((model . ,model) (method . ,method) (args . ,d-args) (kwargs . ,d-kwargs))))
    (odoo/jsonrpc-call-with-decode (concat host path) data)))

(defun odoo/create (host model params &optional kwargs)
  (odoo/call-kw host model "create" (vector params) kwargs))

(defun odoo/search (host model &optional domain)
  (let ((d-domain (if domain domain [])))
    (odoo/call-kw host model "search" (vector d-domain))))

(defun odoo/search-read (host model &optional domain fields)
  (let ((d-domain (if domain domain []))
        (d-fields (if fields fields [])))
    (odoo/call-kw host model "search_read" (vector d-domain d-fields))))

(defun odoo/-unread-messages (host)
  (let* ((domain [["needaction" "=" t]]))
    (odoo/call-kw host "mail.message" "message_fetch" (vector domain))))

(defun odoo/-unread-messages-for-model (host model)
  (let ((messages (odoo/-unread-messages host)))
    (seq-filter
     (lambda (m)
       (equal model (odoo/-get-assoc 'model m)))
     messages)))

(defun odoo/-message-entry (message)
  (let* ((model (odoo/-get-assoc 'model message))
         (res_id (odoo/-get-assoc 'res_id message))
         (title (odoo/-get-assoc 'record_name message))
         (body (odoo/-get-assoc 'body message))
         (message-text (format "[%s :: %d] %s -> %s" model res_id title body)))
    `(,message-text . (,model . ,res_id))))

(defun odoo/-messages-menu (messages &optional action)
  (let* ((data (seq-map #'odoo/-message-entry messages))
         (keymap
           (let ((map (make-sparse-keymap)))
             (define-key map (kbd "C-.") #'counsel-find-symbol)
             (define-key map (kbd "C-,") #'counsel--info-lookup-symbol)
             map))
         (d-action
          (lambda (c)
            (let ((arg (cdr c)))
              (if action (funcall action arg)
                (message-box "%s" arg))))))
    (ivy-read "messages: " data :action d-action)))

(defun odoo/unread-messages-menu (host)
  (let* ((messages (odoo/-unread-messages host))
         (action
          (lambda (model-res-id)
            (let* ((model (car model-res-id))
                   (res_id (cdr model-res-id))
                   (path (format "/web#id=%d&view_type=form&model=%s" res_id model)))
              (browse-url (concat host path))))))
    (if (< 0 (seq-length messages))
      (odoo/-messages-menu messages action)
      (message "No unread messages"))))

(defun odoo/find-timesheets-with-domain (host domain)
  (let* ((session-info (odoo/get-session-info host))
         (user-id (odoo/-get-assoc 'uid session-info))
         (employee-domain (vector (vector "user_id" "=" user-id)))
         (employee_id (aref (odoo/search host "hr.employee" employee-domain) 0))
         (d-domain (vconcat (vector (vector "employee_id" "=" employee_id)) domain))
         (fields ["id" "date" "project_id" "unit_amount" "user_id" "task_id"])
         (ticks (odoo/search-read host "account.analytic.line" d-domain fields)))
    ticks))

(defun odoo/find-timesheets-for-date (host date)
  (let* ((domain (vector (vector "date" "=" date))))
    (odoo/find-timesheets-with-domain host domain)))

(defun odoo/find-timesheets-for-date-range (host start-date end-date)
  (let* ((domain (vector (vector "date" ">=" start-date)
                         (vector "date" "<=" end-date))))
    (odoo/find-timesheets-with-domain host domain)))

(defun odoo/find-timesheets-for-month (host month)
  (let* ((start-date (concat month "-" "01"))
         (last-day (odoo/-days-in-month-at-time start-date))
         (end-date (concat month "-" (number-to-string last-day))))
    (odoo/find-timesheets-for-date-range host start-date end-date)))

(defun odoo/insert-timesheet (host task-id duration)
  (let* ((model "project.task.addtimesheet.wizard")
         (context (odoo/-alist-to-hash `(("active_id" . ,task-id))))
         (data `((task_id . ,task-id) (duration . ,duration))))
    (let ((tsid (odoo/create host model data `((context . ,context)))))
      (odoo/call-kw host model "add_timesheet" (vector tsid)))))

(defun odoo/ticket-number-from-title (title)
  "Ticket numbers are of form SO0-45007, TITLE should contain that number inside brackets."
  (when (string-match "\\[[A-Z][A-Z0-9]*-\\([0-9]+\\)\\]" title)
    (match-string 1 title)))

(defun odoo/insert-timesheet-at-point (&rest args)
  (interactive)
  ;;(message (format "org el: %s" (org-element-at-point))))
  (let* ((clock (org-element-at-point))
         (clock? (equal 'clock (org-element-type clock))))
    (message (format "clock: %s, clock?: %s" clock clock?))
    (when clock?
      (let* ((ts (org-element-property :value clock))
             (duration-str (org-element-property :duration clock))
             (duration-split (-map 'string-to-number (split-string duration-str ":")))
             (duration (+ (car duration-split) (/ (cadr duration-split) 60.0)))
             (odoo-host (or (plist-get args :host) (odoo/default-host)))
             (odoo-user (or (plist-get args :user) (odoo/default-user)))
             (odoo-password (or (plist-get args :password) (odoo/default-password)))
             (odoo-db (or (plist-get args :db) (odoo/default-db))))
        (save-excursion
          ;; move to :LOGBOOK:
          (org-up-element)
          ;; move to task heading
          (org-up-element)
          (let* ((heading (org-element-at-point))
                 (title (org-element-property :title heading)))
            (message (format "title: %s" title))
            (let ((task-id (odoo/ticket-number-from-title title)))
              (if task-id
                  (progn
                    (message (format "task id: %s, duration: %s" task-id duration))
                    (odoo/login odoo-host odoo-user odoo-password odoo-db)
                    (odoo/insert-timesheet odoo-host task-id duration)))
              (message (format "Title '%s' doesn't match the ticket number regexp" title)))))))))

(provide 'odoo)
;;; odoo.el ends here
